/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require("fs");

import { search } from "./search";
import { data } from "./seedData";
import { CallbackManager } from "./callbackManager";
import { DebugService } from "./utils/index";

const results: Record<string, number>[] = [];

[2, 8, 32, 128, 256, 512, 1024].forEach(n => {
  const startTime = Date.now();
  const debugService = new DebugService(() => null);
  const callbackManager = new CallbackManager();
  const bookmarks = data.bookmarks.slice(0, n);
  const searchResults = search(
    bookmarks,
    "crom",
    debugService,
    0,
    callbackManager,
  );

  const endTime = Date.now();
  results.push({
    "inputLength": bookmarks.length,
    "resultsLength": searchResults.length,
    "invocationsCount": debugService.count,
    "elapsedTime": endTime - startTime,
  });
});

results.forEach(result => {
  console.log(
    `[${result.inputLength}] results: ${result.resultsLength} - invocations: ${result.invocationsCount} - elapsedTime: ${result.elapsedTime}`,
  );
});

fs.writeFileSync(
  "build/benchmark.json",
  JSON.stringify(results, null, 2),
);
