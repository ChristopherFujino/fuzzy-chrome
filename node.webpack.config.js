/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global __dirname, process, module */

const path = require("path");
const webpack = require("webpack");
const nodeExternals = require("webpack-node-externals");

const exportsObject = {
  entry: "./src/node_entrypoint.ts",
  mode: process.env.NODE_ENV,
  externals: [nodeExternals()],
  output: {
    filename: "node.js",
    path: path.resolve(__dirname, "build"),
  },
  plugins: [
    new webpack.DefinePlugin({
      // Use interpolation to make value a JS string
      "NODE_ENV": `"${process.env.NODE_ENV}"`,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.ts$/i,
        exclude: [
          /node_modules/,
        ],
        use: "ts-loader",
      },
    ],
  },
  // Teach webpack how to resolve module imports
  // https://webpack.js.org/configuration/resolve/#root
  resolve: {
    extensions: [ ".js", ".ts" ],
  },
  target: "node",
};

if (process.env.NODE_ENV === "development") {
  console.log("inlining source maps...");
  exportsObject["devtool"] = "inline-source-map";
}

module.exports = exportsObject;
