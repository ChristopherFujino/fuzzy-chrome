# fzzbx

A fuzzy finder extension for Chrome's omnibox.

---

## TODO

* have [findBestResult] return all results, and sort at the end
* read bookmarks in the background
* persist completions to localstorage
* case-insensitive search

## References

* [Chrome Extension Samples](https://developer.chrome.com/extensions/samples)

## Legal

Licensed under Apache 2.0.

Licenses added with the [addlicense](https://github.com/google/addlicense) tool.

Copyright 2020 Google LLC.
