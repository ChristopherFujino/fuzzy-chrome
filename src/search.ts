/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DebugService } from "./utils/index";
import { CallbackManager } from "./callbackManager";

export class SearchResult {
  constructor(query: string, input: string, charIndices: number[]) {
    this.query = query;
    this.input = input;
    this.charIndices = charIndices;
  }
  /** Full result that was matched by the query */
  query: string;
  /** Full input string that was matched. */
  input: string;
  /** Indices of the [input] string where a match exists. */
  charIndices: number[];

  /** For scoring. */
  longestSubstringLength(isMatch: boolean) : number {
    return this.substrings.filter(
      substring => substring.isMatch === isMatch,
    ).reduce(
      (longest, current) => current.length > longest.length ? current : longest,
    ).length;
  }

  /** Description to show as omnibox suggestion, with markup */
  get description() : string {
    return "Fuzzy Chrome - " +
      this.substrings.map(sub => sub.description).join("");
  }

  get substrings() : SubString[] {
    const queue = this.charIndices.slice();
    let isMatching : boolean | null = null;
    let currSub = new SubString(
      0,
      0,
      this.input,
      false,
    );
    const subs = [];
    for (let i = 0; i < this.input.length; i++) {
      if (queue[0] === i) {
        // If this is a match
        if (isMatching === null || !isMatching) {
          isMatching = true;
          const nextSub = currSub.createAfter(0, true);
          subs.push(nextSub);
          currSub = nextSub;
        }
        queue.shift();
      } else {
        // Not a match
        if (isMatching === null || isMatching) {
          isMatching = false;
          const nextSub = currSub.createAfter(0, false);
          subs.push(nextSub);
          currSub = nextSub;
        }
      }
      currSub.extend(1);
    }
    return subs;
  }

  /** Higher means a better match. */
  get score() : number {
    let score = 0;
    score += this.longestSubstringLength(true) * 2;
    score += 3 / this.substrings.filter(sub => sub.isMatch).length;
    return score;
  }

  validate(queryStart: number, queryEnd: number): null | SearchResult {
    const querySlice = this.query.slice(queryStart, queryEnd);
    if (this.charIndices.length !== querySlice.length) {
      return null;
    }
    if (this.substrings.filter(sub => sub.isMatch).join("") !== querySlice) {
      return null;
    }
    return this;
  }

  toString(): string {
    return `description: ${this.description}
charIndices: [${this.charIndices}]
score: ${this.score}
query: ${this.query}`;
  }
}

export class SubString {
  constructor(start: number, end: number, source: string, isMatch: boolean) {
    this.start = start;
    this.end = end;
    this.source = source;
    this.isMatch = isMatch;
  }

  static factory(
    start: number,
    end: number,
    source: string,
    isMatch: boolean,
  ) : SubString | null {
    if (start >= end || source[end] === undefined) {
      return null;
    }
    return new SubString(start, end, source, isMatch);
  }

  start: number;
  end: number;
  source: string;
  isMatch: boolean;

  get length(): number {
    return this.end - this.start;
  }

  get description(): string {
    if (this.isMatch) {
      return `<match>${this.toString()}</match>`;
    } else {
      return `<dim>${this.toString()}</dim>`;
    }
  }

  /** Increase [end] by [n]. */
  extend(n: number) : void {
    this.end += n;
    if (this.end > this.source.length) {
      throw new Error("Attempting to extend too far!");
    }
  }

  /** Create a new [SubString] that begins at the end of [this] SubString. */
  createAfter(length: number, isMatch: boolean) : SubString {
    return new SubString(
      this.end,
      this.end + length,
      this.source,
      isMatch,
    );
  }

  toString(): string {
    return this.source.slice(this.start, this.end);
  }
}

function charsFromString(input: string) : Set<string> {
  const chars = new Set<string>();
  for (let i = 0; i < input.length; i++) {
    chars.add(input[i]);
  }
  return chars;
}

/** Return value contains arrays of indices to [input].
 *
 * Will return null if passed empty [query]. */
function hasMatch(
  query: string,
  queryStart: number,
  queryEnd: number,
  input: string,
  inputStart: number,
  inputEnd: number,
) : Record<string, number[]> | null {
  // No query should mean no match
  if (queryEnd - queryStart <= 0 || inputEnd - inputStart <= 0) {
    return null;
  }
  const lookup : Record<string, number[]> = {};
  const queryChars = charsFromString(query.slice(queryStart));
  let queryCursor = queryStart;

  for (let inputCursor = queryStart; inputCursor < inputEnd; inputCursor++) {
    // Populate lookup with indices.
    if (queryChars.has(input[inputCursor])) {
      if (lookup[input[inputCursor]] === undefined) {
        lookup[input[inputCursor]] = [];
      }
      const arr : number[] = lookup[input[inputCursor]];
      arr.push(inputCursor);
    }
    // Move queryCursor if we got a match.
    if (input[inputCursor] === query[queryCursor]) {
      queryCursor += 1;
    }
  }
  // If cursor points beyond last char, we are successful.
  if (queryCursor >= queryEnd) {
    return lookup;
  }
  // If we reach here, then we did not complete the query.
  return null;
}

// Recursive
function findBestResult(
  query: string,
  queryStart: number,
  queryEnd: number,
  input: string,
  inputStart: number,
  inputEnd: number,
  debugService: DebugService,
  depth: number,
  id: number,
  callbackManager: CallbackManager,
) : SearchResult | null {
  if (id !== callbackManager.lastId) {
    throw new Error(`Newer ID started! current ${id} - last ${callbackManager.lastId}`);
  }
  //logger.log(`Entering with query (${queryStart}, ${queryEnd}) - input (${inputStart}, ${inputEnd})`);
  // Determine if a match can be made in order, return null if not.
  const lookup = hasMatch(
    query,
    queryStart,
    queryEnd,
    input,
    inputStart,
    inputEnd,
  );
  if (lookup === null) {
    return null;
  }
  // From here the goal is to find all possible matches, and return the best.
  const queryChar = query[queryStart];
  const entries = lookup[queryChar].filter(
    num => num >= inputStart && num < inputEnd,
  );
  if (entries === undefined) {
    throw new Error("Should not reach here?");
  }

  const results = entries.map(function (index) {
    const subResult = findBestResult(
      query,
      queryStart + 1,
      queryEnd,
      input,
      index + 1,
      inputEnd,
      debugService.incrementCountAndReturn(),
      depth + 1,
      id,
      callbackManager,
    );
    const recursiveIndices = subResult !== null ? subResult.charIndices : [];
    //debugService.log(`input: ${input} - index: ${index} - recursiveIndices: [${recursiveIndices.join(", ")}]`);
    return new SearchResult(
      query,
      input,
      [index, ...recursiveIndices],
    ).validate(queryStart, queryEnd);
  }).filter(
    result => result !== null,
  ).sort(function (a, b) {
    // If compareFunction(a, b) returns greater than 0, sort b to an index
    // lower than a (i.e. b comes first).
    return (b as SearchResult).score - (a as SearchResult).score;
  });

  if (results.length === 0) {
    return null;
  }
  debugService.log(`Returning results (${results.length}) with: [${results.map(result => result === null ? null : `[${result.charIndices.join(", ")}]`).join(", ")}]`);
  return results[0];
}

// return all words that match targetWord, can be non-contiguous
export function search (
  inputs: string[],
  query: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  debugService: DebugService,
  id: number,
  callbackManager: CallbackManager,
) : SearchResult[] {
  const matchedWords : SearchResult[] = [];
  for (const input of inputs) {
    const result = findBestResult(
      query,
      0,
      query.length,
      input,
      0,
      input.length,
      debugService.incrementCountAndReturn(),
      0,
      id,
      callbackManager,
    );

    if (result !== null) {
      matchedWords.push(result);
    }
  }

  return matchedWords.sort(function (a, b) {
    // If compareFunction(a, b) returns greater than 0, sort b to an index
    // lower than a (i.e. b comes first).
    return b.score - a.score;
  });
}
