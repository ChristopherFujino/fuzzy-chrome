/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { search, SearchResult } from "./search";
import { LoggerSpy } from "./utils/tests";
import { DebugService } from "./utils/index";
import { CallbackManager } from "./callbackManager";

class FakeCallbackManager extends CallbackManager {
  constructor(id: number) {
    super();
    this.lastId = id;
  }

  register(callback: (id: number) => void) : void {
    callback(this.lastId);
  }
}

describe("search()", () => {
  let logger: LoggerSpy;
  let callbackManager: CallbackManager;
  let debugService: DebugService;
  const id = 1;

  beforeEach(() => {
    logger = new LoggerSpy();
    debugService = new DebugService(logger.log);
    callbackManager = new FakeCallbackManager(id);
  });

  describe("matches", () => {
    it("only strings matching all chars", () => {
      const results = search(
        [
          "abcdefg",
        ],
        "cde",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(1);
      const result = results[0];
      expect(result.input).toBe("abcdefg");
      expect(
        result.substrings.map(substring => substring.toString()),
      ).toStrictEqual([
        "ab",
        "cde",
        "fg",
      ]);
      expect(result.longestSubstringLength(true)).toBe(3);
    });

    it("only strings matching all chars", () => {
      const results = search(
        [
          "com/flutter/engine",
          "com/flutter/cocoon",
        ],
        "cocoon",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(1);
      expect(results[0].input).toBe("com/flutter/cocoon");
      expect(results[0].longestSubstringLength(true)).toBe(6);
    });

    it("populates charIndices for non-contiguous substrings", () => {
      const results = search(
        [
          "aba",
        ],
        "aa",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(1);
      expect(results[0].charIndices).toStrictEqual([0, 2]);
    });

    it("multiple matching URLs", () => {
      const results = search(
        [
          "google.com",
          "googoo.net",
          "goggles.biz",
        ],
        "goog",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(2);
    });

    it("always matches chars in order", () => {
      const results = search(
        ["abaa"],
        "aa",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(1);
      const result = results[0];
      expect(result.charIndices).toStrictEqual([2, 3]);
      // Start from second element
      for (let i = 1; i < result.charIndices.length; i++) {
        expect(result.charIndices[i] > result.charIndices[i - 1]).toBe(true);
      }
    });
  });

  describe("sort order", () => {
    it("returns results in stable order for reversed inputs", () => {
      const targets = [
        "abc.query.xyz",
        "que.ry",
        "q-ue.r.y",
      ];
      // Clone
      const forwardResults = search(
        targets,
        "query",
        debugService,
        id,
        callbackManager,
      );
      targets.reverse();
      callbackManager.lastId += 1;
      const reverseResults = search(
        targets,
        "query",
        debugService,
        id + 1,
        callbackManager,
      );
      expect(forwardResults.length).toBe(reverseResults.length);
      expect(forwardResults.length).toBe(targets.length);
      for (let i = 0; i < forwardResults.length; i++) {
        expect(forwardResults[i].description)
          .toBe(reverseResults[i].description);
      }
    });

    it("favors less substrings", () => {
      const results = search(
        ["github.com", "go.cogl.net", "google.com"],
        "google",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(2);
      expect(results.map((result) => result.input))
        .toStrictEqual(["google.com", "go.cogl.net"]);
    });

    it("favors longer substrings", () => {
      const results = search(
        ["root-beer.com", "github.com", "r-ootbe.er"],
        "rootbeer",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(2);
      expect(results[0].input).toBe("r-ootbe.er");
    });

    it("collects the longest contiguous substring", () => {
      const results = search(
        ["elttob.com", "b-o-t.t-l-e-bottle.com"],
        "bottle",
        debugService,
        id,
        callbackManager,
      );
      expect(results.length).toBe(1);
      const result = results[0];
      const matchingSubstrings = result.substrings.filter(sub => sub.isMatch);
      expect(matchingSubstrings).toHaveLength(1);
      expect(matchingSubstrings[0].toString()).toBe("bottle");
      expect(result.substrings.filter(sub => sub.isMatch)[0].toString()).toBe("bottle");
      expect(result.longestSubstringLength(true)).toBe(6);
    });
  });
});

describe("SearchResult", () => {
  describe(".substrings", () => {
    it("returns a single substring", () => {
      const result = new SearchResult(
        "bc",
        "abcd",
        [1, 2],
      );
      expect(result.substrings).toHaveLength(3);
    });

    it("returns multiple substrings", () => {
      const result = new SearchResult(
        "abdeg",
        "abcdefg",
        [0, 1, 3, 4, 6],
      );
      expect(result.substrings).toHaveLength(5);
    });
  });

  describe(".description", () => {
    it("works", () => {
      const result = new SearchResult(
        "bc",
        "abcd",
        [1, 2],
      );
      expect(result.description)
        .toBe("Fuzzy Chrome - <dim>a</dim><match>bc</match><dim>d</dim>");
    });
  });

  describe(".longestSubstringLength", () => {
    it("identifies a single contiguous substring", () => {
      const result = new SearchResult(
        "cd",
        "abcde",
        [2, 3],
      );
      expect(result.longestSubstringLength(true)).toBe(2);
    });

    it("identifies the longest of multiple substrings", () => {
      const result = new SearchResult(
        "acdf",
        "abcdef",
        [0, 2, 3, 5],
      );
      expect(result.longestSubstringLength(true)).toBe(2);
    });
  });
});
