/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { search } from "./search";
import { BookmarksService } from "./bookmarks";
import { CallbackManager } from "./callbackManager";
import { DebugService, NoopDebugService } from "./utils/index";

declare const NODE_ENV: string;

console.log(`Hello from background.js: you are in ${NODE_ENV} mode.`);

const debugService = NODE_ENV === "development"
  ? new DebugService(console.log)
  : new NoopDebugService();

let bookmarks: string[];
const callbackManager = new CallbackManager();

const bookmarksService = new BookmarksService(chrome.bookmarks.getTree);
bookmarksService.getBookmarks((results) => {
  bookmarks = results;
  console.log(`Got ${results.length} results!`);
});

function resetDefaultSuggestion() {
  chrome.omnibox.setDefaultSuggestion({
    "description": "<url><match>fz:</match></url> Fuzzy Chrome",
  });
}

// User has started a keyword input session by typing the extension's keyword.
// This is guaranteed to be sent exactly once per input session, and before any
// onInputChanged events.
chrome.omnibox.onInputStarted.addListener(function () {
  console.log("onInputStarted event fired!");
  resetDefaultSuggestion();
});

chrome.omnibox.onInputChanged.addListener(function(text, suggest) {
  callbackManager.register(function (id: number) {
    const matches = search(
      bookmarks,
      text,
      debugService,
      id,
      callbackManager,
    ).slice(0, 6); // only get the first 6 matches
    console.log("got matches:\n" +
      matches.map(obj =>
        `input: ${obj.description}; charIndices: ${obj.charIndices}`,
      ).join("\n"),
    );
    suggest(
      matches.map(match => ({
        "content": match.input,
        "description": match.description,
      } as chrome.omnibox.SuggestResult)),
    );
  });
});

chrome.omnibox.onInputEntered.addListener(function(text) {
  console.log("onInputEntered event fired!");
  chrome.tabs.create({ url: text });
});

resetDefaultSuggestion();
//chrome.omnibox.onInputCancelled.addListener(function () { /* save state */ });
